import logging
import time
import uuid

from fastapi import FastAPI, Response, status, Request
from pydantic import BaseModel
from typing import Optional

from utils import validate_key, publish_mqtt_message

app = FastAPI()

logging.basicConfig(
    filename="webhook_api.log",
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)s %(threadName)s %(name)s %(message)s",
)
logger = logging.getLogger(__name__)


class IftttData(BaseModel):
    param1: Optional[str] = None
    param2: Optional[str] = None
    param3: Optional[str] = None


@app.middleware("http")
async def log_requests(request: Request, call_next):
    request_id = str(uuid.uuid4())
    logger.info(f"req_id: {request_id}, req_url: {request.url.path}")

    start_time = time.time()
    response = await call_next(request)

    processing_time = (time.time() - start_time) * 1000
    logger.info(
        f"req_id: {request_id}, completed_in: {processing_time}ms, response_status_code: {response.status_code}"
    )
    return response


@app.post("/ifttt/{event_name}")
async def webhook_receiver(
    event_name: str,
    item: IftttData,
    response: Response,
    request: Request,
):
    request_key = request.headers.get("request_key", "")
    if not validate_key(request_key):
        response.status_code = status.HTTP_400_BAD_REQUEST
        logging.warning("Invalid request key provided")
        return {"result": "Bad key"}

    publish_mqtt_message(event_name=event_name, payload_data=dict(item))

    return item  # TODO - return something different?
