import json
import os
import uuid

import paho.mqtt.publish as mqtt_publish


secret_key = os.environ["WEBHOOK_KEY"]
mqtt_broker_ip = os.environ["MQTT_BROKER_IP"]


def validate_key(provided_key: str) -> bool:
    return provided_key == secret_key


def publish_mqtt_message(event_name: str, payload_data: dict) -> None:
    mqtt_publish.single(
        f"/events/{event_name}",
        payload=json.dumps(payload_data),
        hostname=mqtt_broker_ip,
        client_id=uuid.uuid4().hex,
    )
