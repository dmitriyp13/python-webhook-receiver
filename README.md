# Python Webhook Receiver

## Description

The goal of this project is to receive a webhook request from IFTTT (and maybe other services in the future) and do something useful with it, for example forward the data in the webhook to an MQTT broker. In the primary use case, this will be running on a local network (home network) with port forwarding or a reverse proxy

## Badges

To be added

## Installation

### Via Git

```
git clone https://gitlab.com/dmitriyp13/python-webhook-receiver.git
pip install -r requirements.txt
```

### Via Pip

Coming soon

### Via Docker

Coming soon

## Configuration

### Local

Once the code is installed, you need to expose port 443 on your local network either via port forwarding (easy via your router) or via a reverse proxy (more complicated but more robust).

The following environment variables need to be set:
| Variable | Description | Required |
| -------- | ----------- | -------- |
| `WEBHOOK_KEY` | IFTTT shared key for the request | Yes |
| `MQTT_BROKER_IP` | MQTT Broker IP Address | Yes |

### IFTTT

The IFTTT webhook request should be in a specific format:
| Request component | Data |
| ----------------- | ---- |
| URL | https://[your domain.com]/ifttt/[event name] |
| `[event name]` | Can be any string |
| Method | POST |
| Content Type | application/json |
| Additional Headers | `{"request_key": "[webhook_key]"}` |
| Body | `{"param1": "[ingredient 1]", "param2": "[ingredient 2]", "param3": "[ingredient 3]"}` |
| `ingredient 1-3` | Can be any string (special characters must be escaped to be valid JSON) |

## Running
Start the service in a virtual environment via `uvicorn main:app --port 8001 &` 

## Support

If you have issues running this code, feel free to send a [support email](mailto:contact-project+dmitriyp13-python-webhook-receiver-31872216-issue-@incoming.gitlab.com) or file an issue.

## Roadmap

Take a look at the current list of issues to get an idea of where this project is going.

## Contributing

If you're interested in contributing to this project, please let me know (a support email is a great start).

## Authors and acknowledgment

Once we have more than just me working on this project, I'll add your names here.

## License

This project is licensed under GPL V3.

## Project status

This project is actively being developed.
