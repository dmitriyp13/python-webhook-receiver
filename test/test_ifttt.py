import sys
import os
from fastapi.testclient import TestClient
import pytest

# getting the name of the directory
# where this file is present.
current = os.path.dirname(os.path.realpath(__file__))

# Getting the parent directory name
# where the current directory is present.
parent = os.path.dirname(current)

# adding the parent directory to
# the sys.path.
sys.path.append(parent)

from main import app

client = TestClient(app)
full_test_data = {"param1": "some value", "param2": "another value", "param3": 123}
partial_test_data = {"param2": "data is beautiful", "param3": 42}


def mocked_mqtt_publish() -> None:
    # def mocked_mqtt_publish(_: str, _a: dict) -> None:
    return None


def test_read_ifttt_event_endpoint_without_event_name():
    # should result in a page not found error (404) since the event name is required
    response = client.get("/ifttt/")
    assert response.status_code == 404


def test_read_ifttt_event_endpoint():
    # get requests are not allowed (405 error)
    response = client.get("/ifttt/some-event")
    assert response.status_code == 405


def test_post_ifttt_event_all_fields():
    # properly formatted request
    response = client.post(
        "/ifttt/my-test-event",
        headers={"request_key": "abc"},
        json=full_test_data,
    )
    assert response.status_code == 200

    # compare contents of returned data
    response_payload = response.json()
    for dict_key in response_payload.keys():
        assert response_payload[dict_key] == str(
            full_test_data[dict_key]
        )  # json converts numerical data to str


def test_post_ifttt_event_some_fields():

    response = client.post(
        "/ifttt/my-test-event",
        headers={"request_key": "abc"},
        json=partial_test_data,
    )
    assert response.status_code == 200

    # compare contents of returned data
    response_payload = response.json()
    for dict_key in response_payload.keys():
        if response_payload[dict_key] is not None:
            assert response_payload[dict_key] == str(
                partial_test_data[dict_key]
            )  # json converts numerical data to str


def test_post_ifttt_event_bad_token():
    response = client.post(
        "/ifttt/my-test-event",
        headers={"request-key": "bad_token"},
        json=full_test_data,
    )
    assert response.status_code == 400
    assert response.json() == {"result": "Bad key"}


def test_post_ifttt_event_missing_token():
    response = client.post(
        "/ifttt/my-test-event",
        json=full_test_data,
    )
    assert response.status_code == 400
    assert response.json() == {"result": "Bad key"}
